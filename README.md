# Pipeline for indoor localization

This module allows to train area localization models as proposed in the paper [...].
The process of data preprocessing, model building and evaluation is encapsulated in the
concept of a `pipeline`. 

A pipeline contains
- a **data component**, which provides the relevant fingerprinting data,
- a **segmentation component** that computes a floor plan segmentation and labels the data with the resulting areas
- a **model component** that represents a machine learning model, which is trained to predict the unknown area given an unlabeled fingerprint

## Submodules

The package utilizes the following submodules:
- [**indoor-loc-data**](https://git.rwth-aachen.de/marius.laska/indoor-loc-data): provides a data providers for [public open dataset](https://zenodo.org/record/889798#.Xd58qWsrK90), which is among others used in the paper. 
- [**indoor-loc-segmentation**](https://git.rwth-aachen.de/marius.laska/ldce-clustering): provides concepts to segment a floor plan based on available training data and hosts an implementation of the LDCE algorithm proposed in [...]
- [**indoor-loc-acs**](https://git.rwth-aachen.de/marius.laska/area-classification-score): provides the implementation of the areas classification score (ACS) metric to evaluate the performance of the trained area classifiers.

## Installation

Installation inside a virtual environment is required. The package can be installed via:
```shell script
(venv) pip install git+https://git.rwth-aachen.de/marius.laska/indoor-loc-pipeline.git
```
A working system installation of tensorflow is mandatory.
Note that the LDCE algorithm implemented in `ìndoor-loc-segmentation` requires a `rtree` wrapper that requires an installation of [libspatialindex](https://libspatialindex.org/).
Installation on linux:
```shell script
sudo apt-get install libspatialindex-dev
```
Installation on Mac:
```shell script
brew install spatialindex
```

## Basic usage

### Configuration
The configuration of a pipeline is supplied as `.yml` file. An example file is included in the `data` folder of the module.

```yaml
data:
  # The data provider which should be used
  provider: LohanDSprovider
  # File name of floor plan img
  floor_plan_img: <image_name>
  # (train, val, test) test=0.2 => 5 folds
  split_ratio: [0.7, 0.1, 0.2]

#
# are used when not locally set for pipeline
#

global_params:

  # number of experiment repetitions
  repetitions: 5

  preprocessing:
    # Whether to standardize the RSS values
    standardize: True
    # Whether to assign labels with no matching area to closet area
    assign_closest: False
    # The floor number of the Lohan dataset, which should be used
    floor: 0
    # How to check for area matches of labels (to label positions with matching areas)
    area_assignment: convex_hull

#
# List of pipelines that are executed
#

pipelines:

  - name: test_pipeline
    floor_plan:
      # 'segmentation' => computes floor plan segmentation,
      #  'regression' => uses DBSCAN to partitions labels into train, test split
      type: segmentation
      # file where segmentation is stored or loaded from
      seg_file: evaluation/cs_seg(100,80)
      # whether to 'load' or compute and 'store' the segmentation
      mode: store # (load/store)
      # file location that holds numpy array with walls (rows of form [s_x, s_y, e_x, e_y]
      walls_file: /home/laskama/PycharmProjects/ILpipeline/venv/lib/python3.6/site-packages/data/lohan/LohanWalls1stfloor.npy
      # file location that holds a numpy array with the points of the outer building walls
      building_shell_file: lohan/building_shell_1stfloor.npy
      # parameters of floor plan segmentation algorithm
      segmentation:
          # algorithm identifier (currently only LDCE supported)
          type: LDCE
          # file location of precomputed distance file (will be recomputed if it does not exist yet)
          precomp_dist_file: evaluation/precomp_dist_e5(2,5).npy
          # Parameters of LDCE algorithm
          stop_size: 100
          eps: 5.0
          max_eps: 50
          min_pts: 3
          min_members: 80
          rss_penalty: 2
          wall_penalty: 5
          cluster_penalty: 20
          area_mode: concave_hull
    preprocessing:
    # model parameters
    model_params:
      type: DNN # (DNN, CNN, kNN, SVM) supported (require different parameters)
      first_neuron: 512
      hidden_layers: 3
      lr: 0.7
      batch_size: 32
      epochs: 200
      dropout: 0.2
      regularization_penalty: 0
      augmentation:

# base directories for file storage
output:
    model_dir: evaluation/test/output/
    summary_dir: evaluation/test/summary/
    img_folder: evaluation/test/ # folder where floorplan images is located (if not present, will be downloaded)
```
The `data` section covers which data provider is used (for example *LohanDSprovider*). Based on the data provider, 
different parameters have to be supplied.
All providers require a `floor_plan_img` and a `split_ratio`. 
The `global_params` section allows to set standard values for the following pipeline specification, which are used in case
no specific values are configured in the pipeline configurations.
A general explanation of the parameters is mentioned in the example .yml file. 
Finally, the `output` section specifies the folders that are constructed to store data during pipeline execution.

#### Floor classification
(currently only supported for GIA dataset provider)
- specify the `map_id` as list: `map_id: [1, 18, 21]` of the maps which should be classified and set the floor plan parameter as 
```yaml
floor_plan:
      type: floor_classification
```

#### Regression vs classification
Models can be trained to predict the precise position (regression) or to predict the right area of the underlying 
floor plan segmentation. In case of regression models, we have to set up a train/test split with
dedicated test positions. Therefore, we utilize the DBSCAN result of the LDCE preprocessing. 
20% of the dense clusters are chosen as testing locations in each fold (5 folds). 
To achieve this preprocessing, the `type` parameter of the `floor_plan` section has to be set
as follows:
```yaml
global_params:
    ...
    floor_plan:
        type: regression
    ...
```
This can be done either globally or for a specific pipeline. 
To achieve a train/test split for area classification, the parameter has to be set to
```yaml
global_params:
    ...
    floor_plan:
        type: segmentation
    ...
```
which is the standard setting, if no value is supplied.
Models that allow for producing a regression or a classification output (DNN, kNN) can 
be configured via the ```pred``` parameter as follows:
```yaml
global_params:
    ...
    model_params:
      pred: classification # or regression
    ...
```
Be careful to always set the prediction mode according to the data splitting mode.

#### Model parameters
The module provides access to 4 different models. For classification, we provide a:
- DNN [keras] (customizable shape)
- CNN [keras] ([fixed architecture](https://www.mdpi.com/2079-9292/8/5/554))
- CNN-PSSD [keras] (Pairwise signal strength difference), use with ```pearsoncorr:>=0.2``` to avoid large image size
- kNN [custom] (weighted avg, k is customizable)
- SVM [sklearn] (all parameters configurable as for [svm.SVC()](https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html))
- transfer [keras] (uses a .hdf5 model file of a trained model. Adds a calibration layer (sparse)). For local fine tuning towards own data/sensors.

The DNN architecture allows for the following parameters:
```yaml
model_params:
    type: DNN
    first_neuron: 256
    hidden_layers: 2
    lr: 0.7 # relative to optimizer (1 equals base lr)
    batch_size: 32
    epochs: 200
    regularization_penalty: 0.05
    augmentation: 3
```
The `augmentation` parameter allows for artificially enlarging the training data (inside the data_generator)
function during model training. The [algorithm](https://www.mdpi.com/2079-9292/8/5/554) used for augmentation is proposed by Sinha et al.
except that data is augmentated inside the area instead of for each label position. 

If a `transfer` model is used. Parameters look as follows:
```yaml
model_params:
    type: transfer
    transfer_model_file: /path/to/pretrained/model/DNN.hdf5
    pred: classification
    
    lr: 0.7
    batch_size: 32
    epochs: 200
```

#### Floor plan segmentation
A detailed description of the applicable parameters for segmentation of the floor plan can
be found in the submodule [**indoor-loc-segmentation**](https://git.rwth-aachen.de/marius.laska/ldce-clustering) that
provides the implementation of the LDCE algorithm.

### Execution
In order to execute a pipeline specified in a .yml file, the following has to be done:
```python
from pkg_resources import resource_filename
from il_pipeline.pipeline import execute

# supply any other filename here
filename = resource_filename('il_pipeline', 'data/test_pipeline.yml')

execute(conf_file=filename)
```

## Evaluation
After a pipeline has been executed it is stored with the supplied `name` parameter in the `model_dir`.
The `plotting` submodule provides several analysis options to visualize the results of the trained pipeline.

It is possible to visualize the class based F_1 score that is achieved during k-fold cross validation as follows:
```python
from pkg_resources import resource_filename
from il_pipeline.plotting.pipeline_plots import floorplan_plot
from il_pipeline.utility.storable import Storable
from il_pipeline.pipeline import Pipeline
from ldce.plotting.floorplan_plot import FloorplanPlot

# supply any other stored pipeline file location here
filename = resource_filename('il_pipeline', 'data/test_pipeline')
pipe: Pipeline = Storable.load(filename)

# generate floor plan plot
fp_img_file = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')
fp = FloorplanPlot(
    floorplan_dimensions=(200, 80), grid_size=4,
    floorplan_bg_img=fp_img_file)

floorplan_plot(pipe, floor_plotter=fp)
```
and generates the image:
![alt text](il_pipeline/data/f1_score.png)

Apart from that it is possible to visualize the training data distribution via:
```python
from il_pipeline.plotting.pipeline_plots import plot_data_heatmap
...
plot_data_heatmap(pipe, floor_plotter=fp)
...
```
which generates the image:
![alt text](il_pipeline/data/data_heatmap.png)

#### Uploading models
Models can be uploaded via calling the `il_pipeline.upload.upload_new.py` file with a .yml
file as:
```yaml
connection:
    base_url: https://indoor-localization.herokuapp.com/
    token: <token>

models:
    floor: gia/upload/floor/output/gia_floor_class[1].hdf5
    maps:
      - id: 1
        area: gia/upload/particle/output/gia_upload_class[1].hdf5
        position: gia/upload/particle/output/gia_upload_reg[1].hdf5
        box: ...

summaries:
    floor: gia/upload/floor/output/gia_floor_class
    maps:
      - id: 1
        area: gia/upload/particle/output/gia_upload_class
        position: gia/upload/particle/output/gia_upload_reg
        box: ...

directories:
    output: /home/laskama/PycharmProjects/ILpipeline/il_pipeline/evaluation/gia/upload/serialized/
    models: evaluation/models/
    summaries: evaluation/summaries/

```