import logging
import numpy as np
from ldce.plotting.floorplan_plot import FloorplanPlot
from pkg_resources import resource_filename
from tqdm import tqdm

from il_pipeline.models.transfer_model import TransferModel
from il_pipeline.summary.summary import KFoldClassSummary
from il_pipeline.models.dnn_model import DnnModel
from il_pipeline.models.knn_model import KnnModel
from il_pipeline.models.svm_model import SvmModel

from ldce.base import ClusterBase
from ldce.clustering import LDCE

from data.data_provider_base import DataProviderBase
from data.gia.gia_data_provider import PDdataProvider
from data.lohan.lohan_data_provider import LohanDSprovider

from il_pipeline.utility.pbarlogging import std_out_err_redirect_tqdm
from il_pipeline.utility.storable import Storable
from il_pipeline.utility.config_reader import ConfigReader

import os
import copy


from debug_tools.logger import getLogger
log = getLogger(level=logging.INFO)

TIME_FORMAT = '%Y-%m-%d %H:%M:%S'


class Pipeline(Storable):
    """
    Class that represents a training il_pipeline including preprocessing via
    a dataset provider and evaluation on test data
    """
    def __init__(self, data_provider: DataProviderBase=None,
                 clusterer: ClusterBase=None, config: ConfigReader=None,
                 params=None, filename=None):
        """
        Construction of Pipeline
        :param data_provider: provides Keras data generator function
        :param clusterer: provides area extents of the floor plan segmentation
        :param output_dir: directory where files are stored
        :param params: DNN model parameters
        """
        self.data_provider = data_provider
        self.clusterer = clusterer
        self.config = config
        self.filename = filename
        self.model_params = params
        self.summary: KFoldClassSummary = None

    #
    # Model training and evaluation methods
    #

    def k_fold_validation(self, data_provider=None) -> KFoldClassSummary:
        """
        Iterates over all folds of the data provider and trains and evaluates
        model for each split. Results are stores in KFoldClassSummary object.
        :param params: optional parameters if different from class value
        :param data_provider: optional data provider if different from class value
        :return: KFoldClassSummary that holds results
        """
        if data_provider is None:
            data_provider = self.data_provider

        self.summary = KFoldClassSummary([], [], [])

        log.info("Training model on {} folds".format(data_provider.num_splits))

        with std_out_err_redirect_tqdm(log) as outputstream:

            for split_idx in tqdm(
                    range(data_provider.num_splits), file=outputstream,
                    dynamic_ncols=True):
                data_provider.current_split_idx = split_idx
                self.train_model()

    def train_model(self):
        params = self.model_params

        m_type = params['pred'] if 'pred' in params else "classification"

        if params['type'] == 'transfer':
            model = TransferModel(m_type, self.summary, self.data_provider, params,
                     self.config.output_dir, self.filename, params['transfer_model_file'])
            model.setup_model()
            model.add_calibration_layer()
            model.set_transfer_model_trainable(False)
            model.train_model()

        if params['type'] == "DNN":
            model = DnnModel(m_type, self.summary, self.data_provider, params,
                     self.config.output_dir, self.filename)
            model.setup_model()
            model.train_model()
        elif params['type'] == "CNN":
            model = DnnModel(params['type'], self.summary, self.data_provider, params,
                     self.config.output_dir, self.filename)
            model.setup_model()
            model.train_model()
        elif params['type'] == "SVM":
            SvmModel(self.summary, self.data_provider, self.model_params, self.config.output_dir,
                     self.filename).train_model()
        elif params['type'] == "kNN":
            KnnModel(m_type, self.data_provider, self.clusterer, self.model_params,
                     self.summary).train_model()

    #
    # Obtain data provider
    #

    @classmethod
    def get_data_provider(cls, data_params, pre_params):
        if data_params['provider'] == "PDdataProvider":
            dp = PDdataProvider.setup_data_provider(
                data_params=data_params,
                pre_params=pre_params,
            )

        elif data_params['provider'] == "LohanDSprovider":
            dp = LohanDSprovider.setup_data_provider(
                data_params=data_params,
                pre_params=pre_params
            )
        else:
            dp = None

        return dp

    #
    # Segmentate floor plan or load existing clusterer
    #

    @classmethod
    def get_floor_plan_segmentation(cls, dataset_provider: DataProviderBase, params):

        if params['type'] == 'floor_classification':
            return None

        if params['mode'] == "load":
            clusterer: ClusterBase = Storable.load(params['seg_file'])
            return clusterer
        else:
            seg_p = params['segmentation']

            walls = None
            if 'walls_file' in params and params['walls_file'] is not None:
                try:
                    walls = np.load(params['walls_file'])
                except FileNotFoundError:
                    walls_file = resource_filename('data', params['walls_file'])
                    try:
                        walls = np.load(walls_file)
                    except FileNotFoundError:
                        walls = None

            try:
                building_shell = np.load(params['building_shell_file'])
            except FileNotFoundError:
                try:
                    building_shell_file = resource_filename('data', params['building_shell_file'])
                    building_shell = np.load(building_shell_file)
                except FileNotFoundError:
                    building_shell = None
            except KeyError:
                building_shell = None

            p = {
                "initial_eps": seg_p['eps'],
                "max_eps": seg_p['max_eps'],
                "max_cluster_dist": 20,
                "step_size": 0.5,
                "min_members": seg_p['min_members'],
                "min_pts": seg_p['min_pts'],
                "wall_penalty": seg_p['wall_penalty'],
            }

            if "precomp_dist_file" in seg_p:
                p["precomp_dist_file"] = seg_p['precomp_dist_file']

            if seg_p['type'] == "LDCE":

                p.update({"stop_growing_sizes": seg_p['stop_size'],
                          "cluster_penalty": seg_p['cluster_penalty'],
                          "rss_penalty": seg_p['rss_penalty'],
                          "area_mode": seg_p['area_mode']})

                clusterer: LDCE = LDCE(p)

                if params['type'] == 'regression':
                    # only determine DBSCAN clusters
                    # Run DBSCAN with precomputed distances to obtain initial clusters
                    base_clusters, l_mappings = clusterer.get_initial_clusters(
                        dataset_provider.labels,
                        clusterer.params['initial_eps'],
                        clusterer.params['min_pts'])

                    clusterer.base_cluster_mappings = l_mappings
                elif params['type'] == 'segmentation':
                    clusterer.ldce_floorplan_segmentation(
                        dataset_provider.labels, dataset_provider.data_tensor,
                        walls, building_shell)

        if params['mode'] == "store":
            clusterer.store(params['seg_file'])

        return clusterer

    @staticmethod
    def merge_summaries(files, new_filename=None):
        y_pred = []
        y_true = []
        y_true_labels = []

        for f in files:
            pipe = Storable.load(f)
            y_pred += pipe.summary.y_pred
            y_true += pipe.summary.y_true
            y_true_labels += pipe.summary.y_true_labels

            os.remove(f)

        pipe.summary.y_pred = y_pred
        pipe.summary.y_true = y_true
        pipe.summary.y_true_labels = y_true_labels
        pipe.num_folds = len(y_pred)

        if new_filename is not None:
            pipe.store(new_filename)

        return pipe

    def store(self, filename=None):
        if filename is None:
            filename = self.filename
        super().store(self.config.output_dir + filename)


def execute(conf_file):
    area_classification = True
    logging.basicConfig(level="INFO")

    conf = ConfigReader(conf_file)

    conf.setup_directories()

    # only PD
    conf.download_floor_plan()

    # sequentially execute all training pipelines
    for p_idx, pipeline_params in enumerate(conf.pipelines):
        num_iterations = conf.get_params('repetitions', pipeline_params)
        log.info('Train and evaluate il_pipeline "{}"... ({}/{}))'.format(
            pipeline_params['name'], p_idx + 1, len(conf.pipelines)))
        log.info('Repeat {} time(s) and build average...'.format(num_iterations))

        # read pipeline parameters
        pre_params = conf.get_params('preprocessing', pipeline_params, merge_level=1)
        model_params = conf.get_params('model_params', pipeline_params, merge_level=1)
        fp_params = conf.get_params('floor_plan', pipeline_params, merge_level=1)
        assign_closest = conf.get_params('assign_closest', pre_params, 'preprocessing')
        area_mode = conf.get_params('area_assignment', pre_params, 'preprocessing')

        # get data provider
        base_data_provider = Pipeline.get_data_provider(conf.data_params, pre_params)

        img = "/home/laskama/PycharmProjects/ILpipeline/il_pipeline/evaluation/gia/gia_floor_3.jpg"
        fp = FloorplanPlot((83.32, 17.16), 2, floorplan_bg_img=img)
        fp.draw_points(base_data_provider.labels[:, 0], base_data_provider.labels[:, 1])

        # for storing pipeline iteration names
        p_names = []

        for run in range(num_iterations):
            pipe_params = copy.deepcopy(pipeline_params)

            p_name = pipe_params['name'] + "[{}]".format(run + 1)
            pipe_params['name'] = p_name
            p_names.append(p_name)

            data_provider = copy.deepcopy(base_data_provider)

            clusterer: ClusterBase = Pipeline.get_floor_plan_segmentation(
                data_provider, fp_params)

            fp.draw_polygons(clusterer.areas)
            fp.draw_clusters(clusterer.base_clusters)
            fp.show_plot()

            # either use segmented floor plan or apply regression
            if fp_params['type'] == 'segmentation':

                area_labels, _, coverage, mask = clusterer.get_cluster_labels_for_areas(
                    data_provider.labels, area_mode=area_mode,
                    assign_closest=assign_closest)

                data_provider.set_area_labels(area_labels,
                                              delete_uncovered=True,
                                              pre_params=pre_params)

                data_provider.remove_APs_with_low_correlation_to_areas(
                    pre_params)

                if data_provider.get_data_dims(
                        model_type="classification")[1] < 2:
                    log.info("Skipping segmentation (only single class)")
                    continue

            elif fp_params['type'] == 'regression':
                #if data_provider.splits_indices is None:
                data_provider.generate_regression_split_indices_from_clusters(
                    clusterer.base_cluster_mappings)

            elif fp_params['type'] == 'floor_classification':
                data_provider.area_labels = data_provider.labels

            else:
                log.error("'type' parameter of 'floor_plan' should be "
                          "(regression or segmentation), but was {} instead".
                          format(fp_params['type']))

            pipeline = Pipeline(data_provider,
                                clusterer,
                                conf,
                                model_params,
                                pipe_params['name'])

            if model_params is not None:
                pipeline.k_fold_validation()

            pipeline.store()

        if model_params is not None:
            pipe_files = [conf.output_dir + p_name for p_name in p_names]
            Pipeline.merge_summaries(pipe_files, pipeline_params['name'])


if __name__ == "__main__":
    #p_file = resource_filename('il_pipeline', "data/test_pipeline.yml")
    p_file = "evaluation/gia/gia_floor3.yml"
    execute(conf_file=p_file)

