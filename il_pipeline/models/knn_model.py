from il_pipeline.summary.summary import KFoldClassSummary
from data.data_provider_base import DataProviderBase
import numpy as np
from ldce.base import ClusterBase


class KnnModel:
    def __init__(self, model_type="classification", dataset_provider: DataProviderBase=None, clusterer: ClusterBase=None, params=None, summary: KFoldClassSummary=None):
        self.model_type = model_type
        self.data_provider = dataset_provider
        self.prediction_vec = None
        self.params = params
        self.clusterer = clusterer
        self.summary = summary
        self.y_pred = None

    def train_model(self):
        # get dataset
        k = self.params['k']

        train_on_noisy_labels = False
        if 'noisy_labels' in self.params:
            train_on_noisy_labels = True

        train_X, train_Y = self.data_provider.get_train_data(area_labels=False, noisy_labels=train_on_noisy_labels)
        test_X, test_Y = self.data_provider.get_test_data(area_labels=False)

        dist = np.full((len(test_X), len(train_X)), -1)
        k_min = np.full((len(test_X), k), -1, dtype=np.float32)
        k_min_idx = np.full((len(test_X), k), -1)

        for idx_x, x in enumerate(test_X):

            # calculate distance per row
            diff = train_X - x
            dist_x = np.linalg.norm(diff, axis=1)
            dist[idx_x, :] = dist_x

            # calc k smallest distances plus indices
            idx = np.argpartition(dist_x, k)
            k_min_idx[idx_x, :] = idx[:k]
            k_min[idx_x, :] = dist_x[idx[:k]]

        min = k_min[:,0]

        sum = np.sum(k_min, axis=1)

        max_expanded = np.repeat(sum, k).reshape(len(sum), -1)

        weights = k_min / max_expanded

        # compute avg of min k values

        weighted_avg = np.zeros((len(test_Y),2))

        for idx, row in enumerate(train_Y[k_min_idx]):
            weighted_row = np.zeros(row.shape)
            for c_idx in range(row.shape[1]):
                weighted_row[:, c_idx] = np.multiply(row[:, c_idx], weights[idx])
            weighted_avg[idx, :] = np.sum(weighted_row, axis=0)

        self.y_pred = weighted_avg

        self.evaluate_model()

    def evaluate_model(self):
        clusterer = self.clusterer
        data_provider = self.data_provider
        summary = self.summary

        _, y_true_labels = data_provider.get_test_data(area_labels=False)

        if self.model_type == "classification":
            y_pred, _, coverage, mask = clusterer.get_cluster_labels_for_areas(
                self.y_pred, area_mode="convex_hull", assign_closest=True)
        else:
            y_pred = self.y_pred

        if data_provider.area_labels is not None:
            _, y_true = data_provider.get_test_data()
            summary.y_true.append(y_true)

        summary.y_pred.append(y_pred)
        summary.y_true_labels.append(y_true_labels)

        summary.num_folds += 1
