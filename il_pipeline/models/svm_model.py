import numpy as np
import inspect
#from keras import Sequential
from sklearn import svm

from il_pipeline.summary.summary import KFoldClassSummary
from data.data_provider_base import DataProviderBase


class SvmModel:
    def __init__(self, summary: KFoldClassSummary=None,
                 data_provider: DataProviderBase=None,
                 params=None, output_dir=None, filename=None):
        """
        TODO
        :param summary: The KFoldClassSummary object in which the evaluation
            results are stored
        :param data_provider:
        :param params: DNN model parameters
        :param output_dir: directory for file storage during training
        :param filename: filename for model weights
        """
        args, _, _, _, _, _, _ = inspect.getfullargspec(svm.SVC)
        base_params = {'decision_function_shape': 'ovr', 'kernel': 'rbf',
                       'gamma': 'auto', 'C': 7, 'probability': True}
        for k, p in params.items():
            if k in args:
                base_params[k] = p

        self.SVM = svm.SVC(**base_params)  # non-linear svm
        self.summary = summary
        self.data_provider = data_provider
        self.params = params
        self.output_dir = output_dir
        self.filename = filename
        self.classifier: Sequential = None

    def train_model(self):
        """
        Trains a DNN model with the specified parameters using Keras
        fit_generator function. Model is evaluated on separate test data
        """
        # use instance variables if no optional variables are supplied
        data_provider = self.data_provider

        train_on_noisy_labels = False
        if 'noisy_labels' in self.params:
            train_on_noisy_labels = True

        x_train, y_train = data_provider.get_train_data(noisy_labels=train_on_noisy_labels)
        x_val, y_val = data_provider.get_val_data(noisy_labels=train_on_noisy_labels)

        x_train_val = np.concatenate((x_train, x_val), axis=0)
        y_train_val = np.concatenate((y_train, y_val), axis=0)

        if 'noise_percentage' in self.params:
            y_train_val = data_provider.add_class_label_noise(
                y_train_val, self.params['noise_percentage'])

        y_train_val = np.argmax(y_train_val, axis=1)

        self.SVM.fit(x_train_val, y_train_val)  # feed samples and labels

        self.evaluate_model()

    def evaluate_model(self):
        """
        Evaluates the model on separate test data (supplied by data generator)
        :param test_bs: batch size of test data
        """

        data_provider = self.data_provider
        summary = self.summary

        x_test, y_true = data_provider.get_test_data()
        _, y_true_labels = data_provider.get_test_data(area_labels=False)

        y_pred = self.SVM.predict(x_test)

        y_pred_one_hot = np.zeros(y_true.shape)
        for idx, y in enumerate(y_pred):
            y_pred_one_hot[idx, y] = 1

        summary.y_pred.append(y_pred_one_hot)
        summary.y_true.append(y_true)
        summary.y_true_labels.append(y_true_labels)

        summary.num_folds += 1
