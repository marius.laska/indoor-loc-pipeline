from keras import regularizers
from keras.callbacks import ModelCheckpoint, EarlyStopping
from keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Flatten
from keras.models import Sequential
from talos.model.layers import hidden_layers
from talos.model.normalizers import lr_normalizer


def cnn_model_for_generator(metrics):

    def define_cnn_model_for_generator(input_shape, Y_cols,
                                                  params) -> Sequential:

        model = Sequential()

        model.add(Conv2D(16, kernel_size=(3, 3),
                         activation='relu',
                         input_shape=input_shape, name="Conv2D_1", padding="same"))
        model.add(Dropout(params['dropout']))

        model.add(Conv2D(16, (3, 3), activation='relu', name="Conv2D_2", padding="same"))
        model.add(MaxPooling2D(pool_size=(2, 2), padding="same"))
        model.add(Dropout(params['dropout']))

        model.add(Conv2D(8, (3, 3), activation='relu', name="Conv2D_3", padding="same"))
        model.add(MaxPooling2D(pool_size=(2, 2), padding="same"))

        model.add(Flatten())
        model.add(Dense(128, activation='relu'))
        #model.add(Dropout(0.25))
        model.add(Dense(Y_cols, activation=params['last_activation']))

        model.compile(loss=params['losses'],
                      metrics=['accuracy', *metrics],
                      optimizer=params['optimizer'](
                          lr=lr_normalizer(params['lr'], params['optimizer'])))

        return model

    return define_cnn_model_for_generator


def classification_model_for_generator(metrics):

    def define_classification_model_for_generator(X_cols, Y_cols, params) -> Sequential:
        model = Sequential()

        model.add(Dense(params['first_neuron'], input_dim=X_cols,
                        activation=params['activation'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty'])))

        model.add(Dropout(params['dropout']))

        hidden_layers(model, params, X_cols)

        model.add(Dense(Y_cols, activation=params['last_activation'],
                        kernel_initializer=params['kernel_initializer'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty'])))

        # compile the model
        model.compile(loss=params['losses'],
                      metrics=['accuracy', *metrics],
                      optimizer=params['optimizer'](
                          lr=lr_normalizer(params['lr'], params['optimizer'])))

        return model

    return define_classification_model_for_generator

#
# Keras Model Definitions with variable parameters
#

def classification_model(metrics, checkpoint_file_name):

    def define_classification_model(train_X, train_Y, val_X, val_Y, params):

        model = Sequential()

        model.add(Dense(params['first_neuron'], input_dim=train_X.shape[1],
                        activation=params['activation'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty'])))

        model.add(Dropout(params['dropout']))

        hidden_layers(model, params, train_Y.shape[1])

        model.add(Dense(train_Y.shape[1], activation=params['last_activation'],
                        kernel_initializer=params['kernel_initializer'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty'])))

        #
        # callbacks
        #

        # Save the best performing model configuration
        checkpoint = ModelCheckpoint(checkpoint_file_name, verbose=0,
                                     monitor='val_loss',
                                     save_best_only=True, mode='auto')

        # Prevent overfitting (if no improvement on validation set for 60 epochs -> stop
        earlyStopping = EarlyStopping(monitor='val_loss',
                                      patience=100,
                                      verbose=0,
                                      mode='auto')

        # compile the model
        model.compile(loss=params['losses'],
                      metrics=['accuracy', *metrics],
                      optimizer=params['optimizer'](
                          lr=lr_normalizer(params['lr'], params['optimizer'])))

        # loc score callback
        #c_callback = MetricForEpochCallback(val_X, val_Y, 3)

        # train the model
        history = model.fit(train_X, train_Y,
                            epochs=params['epochs'],
                            batch_size=params['batch_size'],
                            verbose=0,
                            callbacks=[earlyStopping, checkpoint],
                            validation_data=(val_X, val_Y))

        return history, model

    return define_classification_model


def regression_model(metrics, checkpoint_file_name):

    def define_regression_model(train_X, train_Y, val_X, val_Y, params):

        #
        # define the DNN model
        #

        model = Sequential()

        model.add(Dense(params['first_neuron'], input_dim=train_X.shape[1],
                        activation=params['activation'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty'])))

        model.add(Dropout(params['dropout']))

        hidden_layers(model, params, 2)

        model.add(Dense(2, activation=params['last_activation'],
                        kernel_initializer=params['kernel_initializer'],
                        kernel_regularizer=regularizers.l2(
                            params['regularization_penalty'])))

        #
        # callbacks
        #

        # Save the best performing model configuration
        checkpoint = ModelCheckpoint(checkpoint_file_name, verbose=0,
                                     monitor='val_loss',
                                     save_best_only=True, mode='auto')

        # Prevent overfitting (if no improvement on validation set for 60 epochs -> stop
        earlyStopping = EarlyStopping(monitor='val_loss',
                                      patience=100,
                                      verbose=0,
                                      mode='auto')

        # compile the model
        model.compile(loss=params['losses'],
                      metrics=['mae', *metrics],
                      optimizer=params['optimizer'](
                      lr=lr_normalizer(params['lr'], params['optimizer'])))

        # train the model
        history = model.fit(train_X, train_Y,
                            epochs=params['epochs'],
                            batch_size=params['batch_size'],
                            verbose=0,
                            callbacks=[earlyStopping, checkpoint],
                            validation_data=(val_X, val_Y))

        return history, model

    return define_regression_model
