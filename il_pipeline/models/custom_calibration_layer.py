import tensorflow as tf
from tensorflow.keras import layers


# class Linear(layers.Layer):
#
#     def __init__(self, units=32):
#         super(Linear, self).__init__(name="calibration_layer")
#         self.units = units
#
#     def build(self, input_shape):
#         self.w = self.add_weight(shape=(input_shape[-1], self.units),
#                                  initializer='random_normal',
#                                  trainable=True)
#         self.b = self.add_weight(shape=(self.units,),
#                                  initializer='random_normal',
#                                  trainable=True)
#
#     def call(self, inputs):
#         return tf.matmul(inputs, self.w) + self.b
#
#     def get_config(self):
#         return {'units': self.units}


class Calibration(layers.Layer):

    def __init__(self, units=32, name="calibration_layer", **kwargs):
        super(Calibration, self).__init__(name=name, **kwargs)
        # w_init = tf.ones_initializer()

        self.w = self.add_weight(name='w', shape=(units,),
                                 initializer='ones',
                                 trainable=True)
        self.b = self.add_weight(name='b', shape=(units,),
                                 initializer='zeros',
                                 trainable=True)

        # self.w = tf.Variable(initial_value=w_init(shape=(units,),
        #                                           dtype='float32'),
        #                      trainable=True)
        # b_init = tf.zeros_initializer()
        # self.b = tf.Variable(initial_value=b_init(shape=(units,),
        #                                           dtype='float32'),
        #                      trainable=True)

    def call(self, inputs):
        return tf.multiply(inputs, self.w) + self.b

    def get_config(self):
        base_config = super(Calibration, self).get_config()
        base_config['w'] = self.w
        base_config['b'] = self.b
        return base_config

    @classmethod
    def from_config(cls, config):
        return cls(**config)
