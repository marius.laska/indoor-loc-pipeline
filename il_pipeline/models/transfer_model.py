from data.data_provider_base import DataProviderBase
from tensorflow import keras
from il_pipeline.models.lr_normalizer import lr_normalizer
from il_pipeline.models.custom_calibration_layer import Calibration

from il_pipeline.models.dnn_model import DnnModel
from il_pipeline.summary.summary import KFoldClassSummary
from il_pipeline.utility.storable import Storable


class TransferModel(DnnModel):

    def __init__(self, type="classification", summary: KFoldClassSummary=None,
                 data_provider: DataProviderBase=None,
                 params=None, output_dir=None, filename=None, transfer_model_file=None):
        super().__init__(type, summary, data_provider, params, output_dir, filename)

        self.transfer_model_file = transfer_model_file
        self.transfer_model = None

    def train_model(self):
        # workaround: weight saving keras model with custom layer not allowed
        # (serialization error of tf.Variable (inside custom layer)
        # => save_weights_only=True in case of custom layers being present
        self._train_model(save_weights_only=True)

    def setup_model(self):
        path = self.transfer_model_file

        self.setup_params()

        self.transfer_model = keras.models.load_model(path)
        self.transfer_model._name = "base_model"

    def add_calibration_layer(self):

        input_dim = self.data_provider.data_tensor.shape[1]

        cal_layer = Calibration(input_dim, name="calibration_layer")

        cal_model = keras.Sequential([
            cal_layer,
            self.transfer_model
        ])

        cal_model.compile(loss=self.params['losses'], optimizer=self.params['optimizer'](
            lr=lr_normalizer(self.params['lr'], self.params['optimizer'])))

        self.classifier = cal_model

    def detach_calibration_layer(self):

        params = self.params

        layers = self.classifier.layers
        base_model = keras.Sequential([layers[i] for i in range(len(layers)) if i != 0])

        base_model.compile(loss=params['losses'], optimizer=params['optimizer'](
            lr=lr_normalizer(params['lr'], params['optimizer'])))

        self.transfer_model.summary()
        base_model.summary()

        self.transfer_model = base_model

    def set_transfer_model_trainable(self, trainable):
        params = self.params
        for l in range(len(self.classifier.layers)):
            if l != 0:
                self.classifier.layers[l].trainable = trainable

        self.classifier.compile(loss=params['losses'], optimizer=params['optimizer'](
            lr=lr_normalizer(params['lr'], params['optimizer'])))

    def set_calibration_layer_trainable(self, trainable):
        params = self.params
        self.classifier.get_layer('calibration_layer').trainable = trainable
        self.classifier.compile(loss=params['losses'], optimizer=params['optimizer'](
            lr=lr_normalizer(params['lr'], params['optimizer'])))


if __name__ == "__main__":
    pass
    # pipe: Pipeline = Storable.load("/home/laskama/PycharmProjects/loc_pipe_test/evaluation/lohan/filter/output/DNN_3")
    #
    # params = {'first_neuron': 992, 'act_fnc': 'relu', 'lr': 0.7, 'batch_size': 32, 'epochs': 200}
    #
    # model = TransferModel("classification", KFoldClassSummary([], [], []), pipe.data_provider, params, "test/", "test", "/home/laskama/PycharmProjects/loc_pipe_test/evaluation/lohan/filter/output/DNN_3[1].hdf5")
    #
    # model.setup_model()
    #
    # model.add_calibration_layer()
    #
    # model.full_model.summary()
    #
    # model.set_transfer_model_trainable(False)
    # model.set_calibration_layer_trainable(False)
    #
    # model.full_model.summary()
    #
    # model.classifier = model.full_model
    #
    # model.train_model()

    #model.detach_calibration_layer()