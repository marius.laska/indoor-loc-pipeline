import logging
import math
import os

from tensorflow.keras import Sequential
from tensorflow.keras.activations import softmax, linear
from tensorflow.keras.losses import mean_squared_error
from tensorflow.keras.backend import categorical_crossentropy, relu, softmax
from tensorflow.keras.callbacks import ModelCheckpoint, EarlyStopping
from tensorflow.keras.optimizers import Adam

from il_pipeline.summary.summary import KFoldClassSummary
from il_pipeline.models.tf_model_definition import classification_model_for_generator, cnn_model_for_generator
from data.data_provider_base import DataProviderBase

log = logging.getLogger(__name__)


class DnnModel:
    def __init__(self, type="classification", summary: KFoldClassSummary=None,
                 data_provider: DataProviderBase=None,
                 params=None, output_dir=None, filename=None):
        """
        TODO
        :param summary: The KFoldClassSummary object in which the evaluation
            results are stored
        :param data_provider:
        :param params: DNN model parameters
        :param output_dir: directory for file storage during training
        :param filename: filename for model weights
        """
        self.type = type
        self.summary = summary
        self.data_provider = data_provider
        self.params = params
        self.output_dir = output_dir
        self.filename = filename
        self.classifier: Sequential = None

    def setup_params(self):
        params = self.params

        # update parameters with standard values for classification model
        params.update({
            'shape': 'brick',
            'weight_regulizer': None,
            'kernel_initializer': 'he_normal',
            'optimizer': Adam,
            'activation': relu})

        if self.type == "classification": # or "CNN" in self.type:
            params.update({'last_activation': softmax,
                           'losses': categorical_crossentropy})
        else: # if self.type == "regression":
            params.update({'last_activation': linear,
                           'losses': mean_squared_error})

    def setup_model(self):
        # use instance variables if no optional variables are supplied
        output_dir = self.output_dir
        data_provider = self.data_provider
        params = self.params

        self.setup_params()

        # obtain Keras classifier (Sequential) for specified parameters
        x_cols, y_cols = data_provider.get_data_dims(self.type)
        if self.type == "regression":
            y_cols = 2

        # obtain the model definition
        if self.type == "CNN":
            classifier_template = cnn_model_for_generator(metrics=[])
            self.classifier = classifier_template(input_shape=x_cols,
                                                  Y_cols=y_cols, params=params)
        else:
            classifier_template = classification_model_for_generator(
                metrics=[])

            self.classifier = classifier_template(
                X_cols=x_cols, Y_cols=y_cols, params=params)

    def train_model(self):
        self._train_model(save_weights_only=False)

    def _train_model(self, save_weights_only=False):
        """
        Trains a DNN model with the specified parameters using Keras
        fit_generator function. Model is evaluated on separate test data
        """
        output_dir = self.output_dir
        data_provider = self.data_provider
        params = self.params

        # self.setup_model()

        # calculate batch sizes (might be smaller than specified batch size if
        # not enough data supplied)
        num_train, num_val, num_test = data_provider.get_train_val_test_num(area_labels=self.type != "regression")

        train_bs = min(num_train, params['batch_size'])
        val_bs = min(num_val, train_bs)
        test_bs = min(num_test, train_bs)

        # calculate the steps per epoch (used by generator) to determine
        # after how many steps a new epoch starts (model has seen all data)
        val_steps_per_epoch = math.ceil(num_val / val_bs)
        num_augs = 0
        if 'augmentation' in params and params['augmentation'] is not None:
            num_augs = params['augmentation']

        train_steps_per_epoch = math.ceil(num_train * (num_augs + 1) / train_bs)

        # setup callbacks
        checkpoint_file_name = output_dir + "{}_f{}.hdf5".format(self.filename, data_provider.current_split_idx)

        # save best performing model parameters
        checkpoint = ModelCheckpoint(checkpoint_file_name, verbose=0,
                                     monitor='val_loss',
                                     save_best_only=True, mode='auto',
                                     save_weights_only=save_weights_only)

        earlyStopping = EarlyStopping(monitor='val_loss',
                                      patience=60,
                                      verbose=1,
                                      mode='auto')

        # obtain generator function from data provider
        generator = data_provider.in_mem_data_generator
        area_labels = self.type == "classification" #or "CNN" in self.type

        noise_percentage = None
        if 'noise_percentage' in self.params:
            noise_percentage = self.params['noise_percentage']

        train_on_noisy_labels = False
        if 'noisy_labels' in self.params:
            train_on_noisy_labels = True

        self.classifier.fit_generator(
            generator(mode='train',
                      model_type=self.type,
                      area_labels=area_labels,
                      augmentation=num_augs,
                      noise_percentage=noise_percentage,
                      noisy_labels=train_on_noisy_labels),
            validation_data=generator(mode='val', model_type=self.type,
                                      area_labels=area_labels,
                                      noisy_labels=train_on_noisy_labels,
                                      batch_size=val_bs),
            validation_steps=val_steps_per_epoch,
            steps_per_epoch=train_steps_per_epoch,
            epochs=params['epochs'],
            callbacks=[earlyStopping, checkpoint],
            verbose=0)

        # evaluate model and store results in summary file
        self.evaluate_model(test_bs)

    # def evaluate_model(self, test_bs):
    #     """
    #     Evaluates the model on separate test data (supplied by data generator)
    #     :param test_bs: batch size of test data
    #     """
    #
    #     data_provider = self.data_provider
    #     summary = self.summary
    #     classifier = self.classifier
    #
    #     path = self.output_dir + "{}.hdf5".format(self.filename)
    #     if os.path.exists(path):
    #         classifier.load_weights(path)
    #
    #     # calculate the amount of steps on the test data before one batch
    #     # is completed (required by generator)
    #     _, _, num_test = data_provider.get_train_val_test_num(area_labels=self.type != "regression")
    #     test_steps_per_epoch = math.ceil(num_test / test_bs)
    #
    #     area_labels = self.type == "classification" or self.type == "cnn"
    #
    #     y_pred_list = []
    #     for idx in range(100):
    #
    #         y_pred = classifier.predict_generator(
    #             data_provider.in_mem_data_generator(
    #                 mode='test', model_type=self.type,
    #                 area_labels=area_labels, batch_size=test_bs),
    #             steps=test_steps_per_epoch,
    #             verbose=0)
    #
    #         y_pred_list.append(y_pred)
    #
    #     #if self.type != "regression":
    #     if data_provider.area_labels is not None:
    #         _, y_true = data_provider.get_test_data()
    #         summary.y_true.append(y_true)
    #
    #     _, y_true_labels = data_provider.get_test_data(area_labels=False)
    #     summary.y_pred.append(y_pred_list)
    #     summary.y_true_labels.append(y_true_labels)
    #
    #     summary.num_folds += 1


    def evaluate_model(self, test_bs, chkpt_file=None):
        """
        Evaluates the model on separate test data (supplied by data generator)
        :param test_bs: batch size of test data
        """

        data_provider = self.data_provider
        summary = self.summary
        classifier = self.classifier

        if chkpt_file is None:
            path = self.output_dir + "{}_f{}.hdf5".format(self.filename, data_provider.current_split_idx)
        else:
            path = chkpt_file

        if os.path.exists(path):
            classifier.load_weights(path)

        # calculate the amount of steps on the test data before one batch
        # is completed (required by generator)
        _, _, num_test = data_provider.get_train_val_test_num(area_labels=self.type != "regression")
        test_steps_per_epoch = math.ceil(num_test / test_bs)

        area_labels = self.type == "classification" #or self.type == "cnn"
        y_pred = classifier.predict_generator(
            data_provider.in_mem_data_generator(
                mode='test', model_type=self.type,
                area_labels=area_labels, batch_size=test_bs),
            steps=test_steps_per_epoch,
            verbose=0)

        #if self.type != "regression":
        y_true = []
        if data_provider.area_labels is not None:
            _, y_true = data_provider.get_test_data()

        if data_provider.grid_labels is not None:
            _, y_true = data_provider.get_test_data(
                labels=data_provider.grid_labels)

        summary.y_true.append(y_true)

        _, y_true_labels = data_provider.get_test_data(area_labels=False)
        summary.y_pred.append(y_pred)
        summary.y_true_labels.append(y_true_labels)

        summary.num_folds += 1
