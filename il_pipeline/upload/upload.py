import argparse

import yaml
import numpy as np

from il_pipeline.backend.backend_manager import BackendManager
from il_pipeline.utility.storable import Storable
#from evaluation.hierarchical_dnn_result import MetaData
from il_pipeline.upload.keras_to_tensorflow_converter import serialize_model
from il_pipeline.pipeline import Pipeline


class ModelUploader:
    def __init__(self, conf_file):
        self.conf_file = conf_file
        self.model_files = None
        self.summaries = None
        self.output_params = None
        self.connection_params = None
        self.backend_manager = None

        self.read_config_file()

    def read_config_file(self):
        with open(self.conf_file, "r") as yml_file:
            cfg = yaml.load(yml_file)
            self.model_files = cfg['models']
            self.summaries = cfg['summaries']
            self.output_params = cfg['output']
            self.connection_params = cfg['connection']

    def create_backend_manager(self):
        self.backend_manager = BackendManager(
            self.connection_params['base_url'],
            self.connection_params['token'],
            model_folder=self.output_params['folder'])

    def upload_validated_models(self):

        c_files = []
        if 'classification' in self.model_files:
            c_files = self.model_files['classification']
        r_files = None
        if 'regression' in self.model_files:
            r_files = self.model_files['regression']
        f_file = None
        if 'floor' in self.model_files:
            f_file = self.model_files['floor']

        m_idx = len(c_files)
        if r_files is None and f_file is None:
            m_idx -= 1

        model_file_names = []
        label_mappings = []

        # serialize floor classifiers
        if f_file is not None:
            eval_data = Storable.load(self.summaries['floor'])
            dataset_provider = eval_data.data_provider
            # serialize model from checkpoint file
            model_file_name = self.model_files['floor']
            serialize_model(
                model_file_name, m_idx, custom_loss_function=False,
                output_folder=self.output_params['folder'],
                fmeasure_acc=False,
                walls_h=None, walls_v=None, batch_size=None,
                serialized_name="floor_{}.pb".format(m_idx))

            model_file_names.append("floor_{}.pb".format(m_idx))
            label_mappings.append(dataset_provider.map_id)
            m_idx -= 1

        # serialize area classifiers
        if 'classification' in self.model_files:
            for c_idx, (model_file_name, summary) in enumerate(zip(self.model_files['classification'], self.summaries['classification'])):

                eval_data: Pipeline = Storable.load(summary)

                # serialize model from checkpoint file
                serialize_model(
                    model_file_name, m_idx, custom_loss_function=False,
                    output_folder=self.output_params['folder'],
                    fmeasure_acc=False,
                    walls_h=None, walls_v=None, batch_size=None,
                    serialized_name="area_{}.pb".format(m_idx))

                model_file_names.append("area_{}.pb".format(m_idx))

                #meta: MetaData = eval_data['meta']
                dataset_provider = eval_data.data_provider
                clusterer = eval_data.clusterer

                # convert back to grid dimensions
                # if dataset_provider.converted_to_meter:
                #     mappings = clusterer.cluster_label_mappings[0]
                #     c_labels = []
                #     for c_idx, cluster in enumerate(mappings):
                #         labels = dataset_provider.transform_labels_from_grid_to_meter(
                #             cluster, grid_size=2, inverse=True, set_converted=False)
                #         c_labels.append(labels)

                label_mappings.append(clusterer.areas)

                m_idx -= 1

        # serialize regressor
        if r_files is not None:
            summary = self.summaries['regression']
            model_file_name = self.model_files['regression']

            eval_data = Storable.load(summary)
            #meta: MetaData = eval_data['meta']
            dataset_provider = eval_data.data_provider

            # serialize model from checkpoint file
            serialize_model(
                model_file_name, m_idx, custom_loss_function=False,
                output_folder=self.output_params['folder'],
                fmeasure_acc=False,
                walls_h=None, walls_v=None, batch_size=None,
                meta_file=None,
                serialized_name="reg_{}.pb".format(m_idx))

            model_file_names.append("reg_{}.pb".format(m_idx))

            m_idx -= 1

        mac_vec = dataset_provider.mac_addresses_df.values
        mac_vec = np.transpose(mac_vec).tolist()[0]
        mean_vec = np.array([])
        std_vec = np.array([])

        #mean_vec = dataset_provider.mean_vec
        #std_vec = dataset_provider.std_vec

        self.backend_manager.create_prediction_chain(
            model_file_names,
            dataset_provider.map_id,
            label_mappings,
            mac_vec,
            mean_vec.tolist(),
            std_vec.tolist())


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Upload trained & validated models')

    parser.add_argument('-c',
                        help="yaml config file")

    args = parser.parse_args()

    up = ModelUploader(args.c)

    up.create_backend_manager()
    up.upload_validated_models()
