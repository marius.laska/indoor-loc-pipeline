import argparse

import yaml
import numpy as np

from il_pipeline.backend.backend_manager import BackendManager
from il_pipeline.utility.storable import Storable
#from evaluation.hierarchical_dnn_result import MetaData
from il_pipeline.upload.keras_to_tensorflow_converter import serialize_model
from il_pipeline.pipeline import Pipeline


class ModelUploader:
    def __init__(self, conf_file):
        self.conf_file = conf_file
        self.model_files = None
        self.summaries = None

        self.directories = None
        self.connection_params = None
        self.backend_manager: BackendManager = None

        self.read_config_file()

    def read_config_file(self):
        with open(self.conf_file, "r") as yml_file:
            cfg = yaml.load(yml_file)
            self.model_files = cfg['models']
            self.summaries = cfg['summaries']
            self.directories = cfg['directories']
            self.connection_params = cfg['connection']

    def create_backend_manager(self):
        self.backend_manager = BackendManager(
            self.connection_params['base_url'],
            self.connection_params['token'],
            model_folder=self.directories['output'])

    def upload_models(self):

        model_file_names = []
        data_providers = []
        clusterers = []

        if 'floor' in self.model_files:
            # serialize floor classifier
            eval_data = Storable.load(self.summaries['floor'])
            dataset_provider = eval_data.data_provider
            # serialize model from checkpoint file
            model_file_name = self.model_files['floor']
            serialize_model(
                model_file_name, 0, custom_loss_function=False,
                output_folder=self.directories['output'],
                fmeasure_acc=False,
                walls_h=None, walls_v=None, batch_size=None,
                serialized_name="floor.pb")

            model_file_names.append("floor.pb")
            data_providers.append(dataset_provider)
            clusterers.append(None)

        if 'maps' in self.model_files:
            for map_file, summary_file in zip(self.model_files['maps'], self.summaries['maps']):
                map_id = map_file['id']

                if 'area' in map_file:
                    area_model = map_file['area']

                    # serialize area classifier
                    eval_data: Pipeline = Storable.load(summary_file['area'])

                    # serialize model from checkpoint file
                    serialize_model(
                        area_model, 0, custom_loss_function=False,
                        output_folder=self.directories['output'],
                        fmeasure_acc=False,
                        walls_h=None, walls_v=None, batch_size=None,
                        serialized_name="area_{}.pb".format(map_id))

                    model_file_names.append("area_{}.pb".format(map_id))

                    clusterer = eval_data.clusterer

                    data_providers.append(eval_data.data_provider)
                    clusterers.append(clusterer)

                if 'box' in map_file:
                    box_model = map_file['box']

                    # serialize box model
                    eval_data = Storable.load(summary_file['box'])

                    serialize_model(
                        box_model, 0, custom_loss_function=False,
                        output_folder=self.directories['output'],
                        fmeasure_acc=False,
                        walls_h=None, walls_v=None, batch_size=None,
                        meta_file=None,
                        serialized_name="box_{}.pb".format(map_id))

                    model_file_names.append("box_{}.pb".format(map_id))
                    data_providers.append(eval_data.data_provider)
                    clusterers.append(None)

                if 'position' in map_file:
                    position_model = map_file['position']

                    # serialize regression model
                    eval_data = Storable.load(summary_file['position'])

                    # serialize model from checkpoint file
                    serialize_model(
                        position_model, 0, custom_loss_function=False,
                        output_folder=self.directories['output'],
                        fmeasure_acc=False,
                        walls_h=None, walls_v=None, batch_size=None,
                        meta_file=None,
                        serialized_name="position_{}.pb".format(map_id))

                    model_file_names.append("position_{}.pb".format(map_id))
                    data_providers.append(eval_data.data_provider)
                    clusterers.append(None)

        self.backend_manager.create_new_prediction_chain(model_file_names, data_providers, clusterers)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description='Upload trained & validated models')

    parser.add_argument('-c',
                        help="yaml config file")

    args = parser.parse_args()

    up = ModelUploader(args.c)

    up.create_backend_manager()
    up.upload_models()
