#!/usr/bin/env python
"""
Copyright (c) 2018, by the Authors: Amir H. Abdi
This script is freely available under the MIT Public License.
Please see the License file in the root for details.
The following code snippet will convert the keras model files
to the freezed .pb tensorflow weight file. The resultant TensorFlow model
holds both the model architecture and its associated weights.
"""

import os
import tensorflow as tf
from pathlib import Path
from absl import app
from absl import flags
from absl import logging

from tensorflow import keras
#import tensorflow.keras.backend.tensorflow_backend as K
import tensorflow.keras.backend as K

from tensorflow.keras.models import model_from_json
import subprocess
import sys

from il_pipeline.upload.freeze import freeze_session
from il_pipeline.utility.storable import Storable
from talos.metrics.keras_metrics import fmeasure_acc
import numpy as np

K.set_learning_phase(0)
FLAGS = flags.FLAGS

flags.DEFINE_string('input_model', None, 'Path to the input model.')
flags.DEFINE_string('input_model_json', None, 'Path to the input model '
                                              'architecture in json format.')
flags.DEFINE_string('output_model', None, 'Path where the converted model will '
                                          'be stored.')
flags.DEFINE_string('walls_h', None, 'horizontal walls')
flags.DEFINE_string('walls_v', None, 'vertical walls')
flags.DEFINE_string('meta_file', None, 'pickled meta file')
flags.DEFINE_integer('batch_size', None, 'batch size for loss function')
flags.DEFINE_boolean('custom_loss_function', None, 'name of custom loss function')
flags.DEFINE_boolean('fmeasure_acc', None, 'if fmeasure metric used for model')
flags.DEFINE_boolean('save_graph_def', False,
                     'Whether to save the graphdef.pbtxt file which contains '
                     'the graph definition in ASCII format.')
flags.DEFINE_string('output_nodes_prefix', None,
                    'If set, the output nodes will be renamed to '
                    '`output_nodes_prefix`+i, where `i` will numerate the '
                    'number of of output nodes of the network.')
flags.DEFINE_boolean('quantize', False,
                     'If set, the resultant TensorFlow graph weights will be '
                     'converted from float into eight-bit equivalents. See '
                     'documentation here: '
                     'https://github.com/tensorflow/tensorflow/tree/master/tensorflow/tools/graph_transforms')
flags.DEFINE_boolean('channels_first', False,
                     'Whether channels are the first dimension of a tensor. '
                     'The default is TensorFlow behaviour where channels are '
                     'the last dimension.')
flags.DEFINE_boolean('output_meta_ckpt', False,
                     'If set to True, exports the model as .meta, .index, and '
                     '.data files, with a checkpoint file. These can be later '
                     'loaded in TensorFlow to continue training.')

flags.mark_flag_as_required('input_model')
flags.mark_flag_as_required('output_model')


def load_model(input_model_path, input_json_path):
    if not Path(input_model_path).exists():
        raise FileNotFoundError(
            'Model file `{}` does not exist.'.format(input_model_path))
    try:
        custom_objects = {}

        if FLAGS.custom_loss_function:
            walls_h = np.array([[5.5, 0.5, 5.5, 28]])
            walls_v = np.array([[5.5, 2.5, 9.5, 2.5],  # O1 <-> O2
                                [5.5, 4.5, 9.5, 4.5],  # O2 <-> O3
                                [5.5, 6.5, 9.5, 6.5]])
            loss_function = custom_wall_loss(walls_h, walls_v,
                                             batch_size=32,
                                             wall_penalty_term=10)

            custom_objects['custom_wall_loss'] = loss_function
        elif FLAGS.fmeasure_acc:
            custom_objects['fmeasure_acc'] = fmeasure_acc

        #custom_objects["euclidean_dist"] = euclidean_dist

        if FLAGS.meta_file:
            data = Storable.load(FLAGS.meta_file)
            meta: MetaData = data['meta']
            walls_h = meta.dataset_provider.walls_h
            walls_v = meta.dataset_provider.walls_v

            wall_loss = custom_wall_loss(walls_h,
                                         walls_v,
                                         batch_size=32,
                                         wall_penalty_term=2,
                                         base_loss="ML2")

            custom_objects["custom_wall_loss"] = wall_loss

        model = keras.models.load_model(input_model_path, custom_objects=custom_objects, compile=False)

        return model
    except FileNotFoundError as err:
        logging.error('Input mode file (%s) does not exist.', FLAGS.input_model)
        raise err
    except ValueError as wrong_file_err:
        if input_json_path:
            if not Path(input_json_path).exists():
                raise FileNotFoundError(
                    'Model description json file `{}` does not exist.'.format(
                        input_json_path))
            try:
                model = model_from_json(open(str(input_json_path)).read())
                model.load_weights(input_model_path)
                return model
            except Exception as err:
                logging.error("Couldn't load model from json.")
                raise err
        else:
            logging.error(
                'Input file specified only holds the weights, and not '
                'the model definition. Save the model using '
                'model.save(filename.h5) which will contain the network '
                'architecture as well as its weights. If the model is '
                'saved using model.save_weights(filename), the flag '
                'input_model_json should also be set to the '
                'architecture which is exported separately in a '
                'json format. Check the keras documentation for more details '
                '(https://keras.io/getting-started/faq/)')
            raise wrong_file_err


def main(args):
    # If output_model path is relative and in cwd, make it absolute from root
    output_model = FLAGS.output_model
    if str(Path(output_model).parent) == '.':
        output_model = str((Path.cwd() / output_model))

    output_fld = Path(output_model).parent
    output_model_name = Path(output_model).name
    output_model_stem = Path(output_model).stem
    output_model_pbtxt_name = output_model_stem + '.pbtxt'

    # Create output directory if it does not exist
    Path(output_model).parent.mkdir(parents=True, exist_ok=True)

    if FLAGS.channels_first:
        K.set_image_data_format('channels_first')
    else:
        K.set_image_data_format('channels_last')

    model = load_model(FLAGS.input_model, FLAGS.input_model_json)

    # # TODO(amirabdi): Support networks with multiple inputs
    orig_output_node_names = [node.op.name for node in model.outputs]
    if FLAGS.output_nodes_prefix:
        num_output = len(orig_output_node_names)
        pred = [None] * num_output
        converted_output_node_names = [None] * num_output

        # Create dummy tf nodes to rename output
        for i in range(num_output):
            converted_output_node_names[i] = '{}{}'.format(
                FLAGS.output_nodes_prefix, i)
            pred[i] = tf.identity(model.outputs[i],
                                  name=converted_output_node_names[i])

    frozen_graph = freeze_session(K.get_session(),
                                  output_names=converted_output_node_names)

    tf.train.write_graph(frozen_graph, str(output_fld), output_model_name,
                         as_text=False)


def serialize_model(checkpoint_file, output_number, custom_loss_function=False,
                    fmeasure_acc=False, meta_file=None, output_folder="output/",
                    walls_h=None, walls_v=None, batch_size=None, serialized_name="gia_-1.pb"):
    fmeasure_flag = "--fmeasure_acc"
    if fmeasure_acc:
        fmeasure_flag += "=true"
    else:
        fmeasure_flag += "=false"

    flags = []

    if meta_file is not None:
        flags.append("--meta_file=" + meta_file)

    dir_path = os.path.dirname(os.path.realpath(__file__))

    if custom_loss_function:
        subprocess.call(
            [sys.executable, dir_path + "/keras_to_tensorflow_converter.py",
             "--input_model={}".format(checkpoint_file),
             "--output_model={}".format(
                 output_folder + serialized_name),
             "--output_nodes_prefix=output",
             "--custom_loss_function=true",
             fmeasure_flag])

    else:
        subprocess.call(
            [sys.executable, dir_path + "/keras_to_tensorflow_converter.py",
             "--input_model={}".format(checkpoint_file),
             "--output_model={}".format(
                 output_folder + serialized_name),
             "--output_nodes_prefix=output",
             fmeasure_flag,
             *flags])


if __name__ == "__main__":
    app.run(main)