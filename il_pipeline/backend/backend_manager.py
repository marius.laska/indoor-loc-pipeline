import requests
import shapely
from shapely.geometry import Polygon
import json
import numpy as np


class BackendManager:

    def __init__(self, base_url, bearer_token, map_id=None, model_folder=None,
                 map_folder=None, map_img_name=None):
        self.base_url = base_url
        self.bearer_token = bearer_token
        self.model_folder = model_folder
        self.map_folder = map_folder
        self.map_id = map_id
        self.map_img_name = map_img_name

    def upload_model(self, model_id, file_name):
        url = self.base_url + 'mlmodel/' + str(model_id) + '/upload'
        files = {'model': open(self.model_folder+file_name, 'rb')}
        headers = {'Authorization': 'Bearer ' + self.bearer_token}
        r = requests.post(url, files=files, headers=headers)


    def get_all_models(self):
        url = self.base_url + 'mlmodel'
        r = requests.get(url)
        return r.json()


    def get_model(self, model_id):
        url = self.base_url + 'mlmodel/' + str(model_id)
        r = requests.get(url)
        return r.json()

    def download_floorplan(self):
        url = self.base_url + 'map/{}/download'.format(self.map_id)
        headers = {'Authorization': 'Bearer ' + self.bearer_token}
        r = requests.get(url, headers=headers)
        with open(self.map_folder + self.map_img_name, 'wb') as img:
            img.write(r.content)

    def create_new_model(self, model_type, model_file_name, model_description, map_id,
                         mac_addresses, mean_scaling_values, std_scaling_values,
                         mean_error, min_error, max_error, var_error):
        url = self.base_url + 'mlmodel/'
        payload = {'modelType': model_type,
                   'modelFileName': model_file_name,
                   'modelDescription': model_description,
                   'map': map_id,
                   'macAddresses': mac_addresses,
                   'meanScalingValues': mean_scaling_values,
                   'stdScalingValues': std_scaling_values,
                   'meanError': mean_error,
                   'minError': min_error,
                   'maxError': max_error,
                   'varError': var_error}

        headers = {'Authorization': 'Bearer ' + self.bearer_token}
        r = requests.post(url, data=payload, headers=headers)
        json_par = r.json()

        if model_file_name:
            self.upload_model(json_par['id'], model_file_name)


    # generate json data of form:
    # "outputSchema": [{"office_1": ["3,6", "3,5", "4,5", "4,6"]},
    #                  {"office_2": ["1,6", "2,6", "1,7"]}]}
    def generate_new_json_output_schema(self, cluster_label_mapping):
        output_schema = []
        for idx, cluster in enumerate(cluster_label_mapping):
            coords = list(cluster.exterior.coords)
            cluster_list = ["{},{}".format(x[0], x[1]) for x in coords]
            output_schema.append({"labelName": "label_{}".format(idx),
                                  "gridCells": cluster_list})

        return output_schema

    def generate_floor_output_schema(self, floor_ids):
        output_schema = []
        for idx, map_id in enumerate(floor_ids):
            output_schema.append({"labelName": "floor_{}".format(idx),
                                  "gridCells": [map_id]})

        return output_schema

    def generate_json_output_schema(self, cluster_label_mapping):
        output_schema = []
        for idx, cluster in enumerate(cluster_label_mapping):
            cluster_list = ["{},{}".format(x[0], x[1]) for x in cluster]
            output_schema.append({"labelName": "label_{}".format(idx), "gridCells": cluster_list})

        return output_schema

    def create_new_prediction_chain(self, model_file_names, data_providers, clusterers):
        headers = {'Authorization': 'Bearer ' + self.bearer_token}
        url = self.base_url + 'model/create'

        payload = []

        for model, data_provider, clusterer in zip(model_file_names, data_providers, clusterers):

            output_schema = None
            level = 0
            map_id = data_provider.map_id

            if 'floor' in model:
                output_schema = self.generate_floor_output_schema(data_provider.map_id)
                level = 2
            elif 'area' in model:
                output_schema = self.generate_new_json_output_schema(clusterer.areas)
                level = 1
            elif 'box' in model:
                output_schema = [{"labelName":"GRID-BBOX", "gridCells": [str(data_provider.grid_size)]}]
                level = 3

            if type(map_id) is list:
                map_id = map_id[0]

            mac_vec = data_provider.mac_addresses_df.values
            mac_vec = np.transpose(mac_vec).tolist()[0]
            mean_vec = []
            std_vec = []

            model_payload = {
                "macAddresses": mac_vec,
                "meanScalingValues": mean_vec,
                "stdScalingValues": std_vec,
                "map": map_id,
                "level": level,
                "outputSchema": output_schema}

            payload.append(model_payload)

        r = requests.post(url, json=payload, headers=headers)
        json_par = r.json()

        models = json_par['models']
        for idx, model in enumerate(models):
            model_id = model['id']
            self.upload_model(model_id, model_file_names[idx])

    def create_prediction_chain(self, model_file_names, map_id, cluster_label_mappings,
                                mac_addresses_vec, mean_vec, std_vec):
        headers = {'Authorization': 'Bearer ' + self.bearer_token}
        url = self.base_url + 'model/create'

        payload = []

        for idx, model in enumerate(model_file_names):

            output_schema = None
            if idx < len(cluster_label_mappings):
                if 'area' in model:
                    output_schema = self.generate_new_json_output_schema(cluster_label_mappings[idx])
                elif 'floor' in model:
                    output_schema = self.generate_floor_output_schema(cluster_label_mappings[idx])

            if type(map_id) is list:
                map_id = map_id[0]
            model_payload = {
                "macAddresses": mac_addresses_vec,
                "meanScalingValues": mean_vec,
                "stdScalingValues": std_vec,
                "map": map_id,
                "level": len(model_file_names)-idx-1,
                "outputSchema": output_schema}

            payload.append(model_payload)

        r = requests.post(url, json=payload, headers=headers)
        json_par = r.json()

        models = json_par['models']
        for idx, model in enumerate(models):
            model_id = model['id']
            self.upload_model(model_id, model_file_names[idx])
