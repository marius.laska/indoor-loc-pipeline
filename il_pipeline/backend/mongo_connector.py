from pymongo import MongoClient
from sqlalchemy import create_engine
import pandas as pd

"""
Meant for deletion of file that are still stored in MongoDB (GridFS)
but have been already deleted in SQL database
"""


def delete_from_mongo(keep_ids, mongo_connect=""):
    mongo = MongoClient(mongo_connect)
    db = mongo["indoor-localization"]

    f_ids = [str(id) for id in db.fs.files.find().distinct('_id')]

    for f_id in f_ids:
        if f_id not in keep_ids and f_id.endswith(".pb"):
            # delete
            res = db.fs.chunks.remove({"files_id": f_id})
            res = db.fs.files.remove({'_id': f_id})
            print("deleted")


def get_existing_ids(sql_connect=""):
    # initiate database connection
    engine = create_engine(sql_connect)

    # get map dimensions to calc total area
    dim_query = """SELECT m."modelFd" FROM mlmodel as m"""
    return pd.read_sql_query(dim_query, con=engine).to_numpy().flatten().tolist()


if __name__ == "__main__":
    keep_ids = get_existing_ids(sql_connect="")
    delete_from_mongo(keep_ids, mongo_connect="")