import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd
from shapely.geometry import box, Polygon
from ldce.plotting.floorplan_plot import FloorplanPlot
from acs.metrics import acs_score_for_precomputed_f1

from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable


def plot_cdf_error_Lohan():
    segs = ["broad", "fine"]

    base = ""

    class_names = ["CNN", "DNN(deep)", "SVM"]
    c_d_names = ["CNN", "DNN", "SVM"]

    reg_names = ["KNN(2)", "KNN(3)", "KNN(5)", "DNN(reg)"]
    r_d_names = ["KNN(2)", "KNN(3)", "KNN(5)", "DNN(reg)"]

    d = {"x": [], "model": [], "y": [], "SEG": []}

    for seg in segs:

        for c_name, c_d_name in zip(class_names, c_d_names):
            pipe: Pipeline = Storable.load(base.format(seg, c_name))

            save_cdf_to_dict(pipe, d, seg, c_d_name,
                             type="class", dist_mode="center")

        for r_name, r_d_name in zip(reg_names, r_d_names):
            pipe: Pipeline = Storable.load(base.format(seg, r_name))

            save_cdf_to_dict(pipe, d, seg, r_d_name, type="reg")

    df = pd.DataFrame(data=d)

    plt.style.use("default")

    sns.set(style="darkgrid")
    sns.set(font_scale=.7)
    plt.figure()

    #set_plt_style_big(label=10, rest=8)

    g = sns.FacetGrid(df, col="SEG", hue="model")
    g.map(sns.lineplot, "x", "y", label="small")

    g.set(xlabel="Distance [m]")
    g.set(ylabel="Fraction of data")
    g.set(xlim=(0, 60))
    g.set(xticks=np.arange(0, 61, 10))
    g.set(ylim=(None, 1))
    g.set(yticks=np.arange(.7, 1.01, 0.05))

    g.add_legend()
    g.savefig(
        "lohan_class_perf.pdf")  # , bbox_inches="tight")

    plt.show()

def generate_table_class_cdf_lohan():
    segs = ["broad"]
    base = ""
    d = {"SEG": [], "model": [], "min": [], "max": [], "mean": [], "std": []}

    class_names = ["CNN", "DNN(deep)", "SVM"]
    reg_names = ["KNN(2)", "KNN(3)", "KNN(5)", "DNN(reg)"]

    # classification models
    for seg in segs:                                                                                                                                                                                                                                                                                                                                                                                                        
        for c_name in class_names:
            p: Pipeline = Storable.load(base.format(seg, c_name))

            d["SEG"].append(seg)
            d["model"].append(c_name)
            d["min"].append(p.summary.get_class_error(p.clusterer, type="min"))
            d["max"].append(p.summary.get_class_error(p.clusterer, type="max"))
            d["mean"].append(p.summary.get_class_error(p.clusterer, type="mean"))
            d["std"].append(p.summary.get_class_error(p.clusterer, type="std"))

    cdf_type = "class"
    for seg in segs:
        for r_name in reg_names:
            p: Pipeline = Storable.load(base.format(seg, r_name))

            d["SEG"].append(seg)
            d["model"].append(r_name)
            d["min"].append(p.summary.get_class_error_for_reg(p.clusterer, type="min", model_type=cdf_type))
            d["max"].append(p.summary.get_class_error_for_reg(p.clusterer, type="max", model_type=cdf_type))
            d["mean"].append(p.summary.get_class_error_for_reg(p.clusterer, type="mean", model_type=cdf_type))
            d["std"].append(p.summary.get_class_error_for_reg(p.clusterer, type="std", model_type=cdf_type))

    df = pd.DataFrame(d)

    return df


def save_cdf_to_dict(pipe, dic, gs, model, type="reg", dist_mode="center"):
    if type == "reg":
        X_unique, Y_unique = get_cdf_values(
            pipe.summary.get_misclass_reg_cdf(
                pipe.clusterer, add_penalty_for_missed_class=False, dist_mode=dist_mode))
    elif type == "class":
        X_unique, Y_unique = get_cdf_values(
            pipe.summary.get_misclass_cdf(pipe.clusterer, dist_mode=dist_mode))
    elif type == "cdf":
        X_unique, Y_unique = get_cdf_values(
            pipe.summary.get_reg_cdf(pipe.data_provider))

    dic["x"] += X_unique.tolist()
    dic["y"] += Y_unique.tolist()
    dic["SEG"] += [gs] * len(X_unique)
    dic["model"] += [model] * len(X_unique)


def get_cdf_values(error):
    N = len(error)
    X = np.sort(error)

    X_unique, indices = np.unique(X, return_index=True)
    indices -= 1
    indices = indices[1:]
    X_unique = X_unique[:-1]

    Y = np.array(range(N)) / float(N)
    Y_unique = Y[indices]

    return X_unique, Y_unique


def plot_cluster_ACS_visualization():
    lambda_val = 0.5
    mu_val = 1
    entire_area = box(0.5, 0.5, 22, 16.5)
    c_pal = sns.color_palette(n_colors=3,
                              palette="deep")

    fp_img_file = "/home/laskama/PycharmProjects/ILpipeline/il_pipeline/plotting/gia_map_no_label.png"

    big_boxes = [box(0.5,0.5,14.7,6.4), #lower offices
                 box(5, 7.3,22, 10), # floor
                 box(5.5,11.2,22,16.5)] # upper offices

    med_boxes = [box(0.5, 0.5, 7, 6.4),  # lower office left
                 box(8.2, 0.5, 14.7, 6.4),  # lower office right
                 box(5, 7.3, 22, 10),  # floor
                 box(5.5, 11.2, 7.2, 16.5),  # upper office left
                 box(8.1, 11.2, 14.8, 16.5),  # upper office middle
                 box(15.3, 11.2, 22, 16.5)]  # upper office right

    small_boxes = [
        # lower offices left-to-right
        box(0.5, 0.5, 3.5, 6.4),
        box(4.3, 0.5, 7, 6.4),
        box(8.2, 0.5, 11, 6.4),
        box(11.8, 0.5, 14.7, 6.4),

        box(5, 7.3, 11, 10),  # floor left
        box(11.8, 7.3, 22, 10),  # floor right

        # upper offices (left-to-right)
        box(5.5, 11.2, 7.2, 16.5),
        box(8.1, 11.2, 11.1, 16.5),
        box(11.6, 11.2, 14.8, 16.5),
        box(15.3, 11.2, 18.4, 16.5),
        box(19.2, 11.2, 22, 16.5)]


    big_f1 = [.95, .93, .91]
    med_f1 = [.9, .92, .93, .89, .91, .88]
    #med_f1 = [.82, .81, .9, .89, .76, .8]
    small_f1 = [.8, .76, .54, .88, .72, .85, .68, .81, .7, .73, .84]

    seg_names = ["broad", "medium", "fine"]
    f1_scores = [big_f1, med_f1, small_f1]
    areas = [big_boxes, med_boxes, small_boxes]

    acs = acs_score_for_precomputed_f1(big_f1, [b.area for b in big_boxes], entire_area.area, lambda_val, mu_val)

    plot_dict = {r'$\lambda$': [], "ACS": [], "SEG": []}

    for l in np.arange(0, 1.01, 0.01):
        for seg_f1, seg_area, seg_name in zip(f1_scores, areas, seg_names):
            plot_dict[r'$\lambda$'].append(l)
            plot_dict["ACS"].append(acs_score_for_precomputed_f1(seg_f1, [b.area for b in seg_area], entire_area.area, l, mu_val))
            plot_dict["SEG"].append(seg_name)

    df = pd.DataFrame(data=plot_dict)

    plt.style.use("default")

    sns.set(style="darkgrid")
    sns.set(font_scale=.7)

    set_plt_style_big(label=14, rest=14)

    plt.figure()
    g = sns.lineplot(x=r'$\lambda$', y="ACS", hue="SEG", data=df)
    g.set(xticks=np.arange(0, 1.01, 0.1))
    g.set(xlim=(0, 1))
    g.set(ylim=(0, None))

    l_broad = g.lines[0]
    l_med = g.lines[1]
    l_fine = g.lines[2]

    x_broad = l_broad.get_xydata()[:, 0]
    mask = np.where(x_broad <= 0.13)[0]
    x_broad = x_broad[mask]
    y_broad = l_broad.get_xydata()[:, 1]
    y_broad = y_broad[mask]

    x_med = l_med.get_xydata()[:, 0]
    mask = np.where(np.logical_and(x_med >= 0.13, x_med <= 0.37))[0]
    x_med = x_med[mask]
    y_med = l_med.get_xydata()[:, 1]
    y_med = y_med[mask]

    x_fine = l_fine.get_xydata()[:, 0]
    mask = np.where(x_fine >= 0.37)[0]
    x_fine = x_fine[mask]
    y_fine = l_fine.get_xydata()[:, 1]
    y_fine = y_fine[mask]

    g.fill_between(x_broad, y_broad, color=c_pal[0], alpha=0.3)
    g.fill_between(x_med, y_med, color=c_pal[1], alpha=0.3)
    g.fill_between(x_fine, y_fine, color=c_pal[2], alpha=0.3)

    g.get_figure().savefig("acs_plot_cmp.pdf",
                           bbox_inches="tight")

    # # # # # # # # # # # # # #
    # broad segmentation plot #
    # # # # # # # # # # # # # #

    fp = FloorplanPlot(
        floorplan_dimensions=(83.32, 17.16), grid_size=2,
        floorplan_bg_img=fp_img_file, filename="acs_seg_vis(broad).pdf")

    polys = [Polygon(b) for b in big_boxes]

    fp.draw_polygons(polys, color=c_pal[0], linewidth=1.5)
    fp.draw_area_text(polys, big_f1)
    fp.save_plot()

    fp = FloorplanPlot(
        floorplan_dimensions=(83.32, 17.16), grid_size=2,
        floorplan_bg_img=fp_img_file, filename="acs_seg_vis(broad).pdf")

    polys = [Polygon(b) for b in big_boxes]

    fp.draw_polygons(polys, color=c_pal[0], linewidth=1.5)
    fp.draw_area_text(polys, big_f1)
    fp.save_plot()

    # # # # # # # # # # # # #
    # med segmentation plot #
    # # # # # # # # # # # # #

    fp = FloorplanPlot(
        floorplan_dimensions=(83.32, 17.16), grid_size=2,
        floorplan_bg_img=fp_img_file, filename="acs_seg_vis(med).pdf")

    polys = [Polygon(b) for b in med_boxes]

    fp.draw_polygons(polys, color=c_pal[1], linewidth=1.5)
    fp.draw_area_text(polys, med_f1)
    fp.save_plot()

    # # # # # # # # # # # # #
    # fine segmentation plot #
    # # # # # # # # # # # # #

    fp = FloorplanPlot(
        floorplan_dimensions=(83.32, 17.16), grid_size=2,
        floorplan_bg_img=fp_img_file, filename="acs_seg_vis(fine).pdf")

    polys = [Polygon(b) for b in small_boxes]

    fp.draw_polygons(polys, color=c_pal[2], linewidth=1.5)
    fp.draw_area_text(polys, small_f1)
    fp.save_plot()

    plt.show()

def set_plt_style_big(label=12, rest=10):
    plt.rcParams["axes.labelsize"] = label
    plt.rcParams["xtick.labelsize"] = rest
    plt.rcParams["ytick.labelsize"] = rest
    plt.rcParams["legend.fontsize"] = rest


if __name__ == "__main__":
    # plot_cluster_ACS_visualization()
    #df = generate_table_class_cdf_lohan()
    #print(df.round(2).to_latex(columns=["model", "mean", "std", "min", "max"]))

    plot_cdf_error_Lohan()