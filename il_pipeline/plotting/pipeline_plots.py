import numpy as np
import matplotlib.pyplot as plt
from ldce.plotting.floorplan_plot import FloorplanPlot
from pkg_resources import resource_filename

from il_pipeline.pipeline import Pipeline
from il_pipeline.utility.storable import Storable
from debug_tools import getLogger
import logging
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

log = getLogger(logging.INFO)


def floorplan_plot(pipe: Pipeline, filename=None, floor_plotter=None):
    """
    Plots the achieved F1 scores per class (during k-fold cross validation)
    on floor plan with predetermined segmentation
    """
    f1_scores = pipe.summary.get_avg_fmeasure()

    floor_plotter.draw_polygons(pipe.clusterer.areas, bbox=False, color='black',
                                linewidth=1.5)
    floor_plotter.draw_area_text(pipe.clusterer.areas,
                                 np.round(f1_scores, decimals=2).tolist(),
                                 color="darkgreen")

    if filename is not None:
        floor_plotter.filename = filename
        floor_plotter.save_plot()


def plot_cluster_number(pipe: Pipeline, floor_plotter=None, show_number=True):

    floor_plotter.draw_polygons(pipe.clusterer.areas, bbox=False, linewidth=2.5,
                                color=None, linestyle="-")

    if show_number:
        floor_plotter.draw_area_text(pipe.clusterer.areas,
                                     range(len(pipe.clusterer.areas)))


def plot_data_heatmap(pipe: Pipeline, filename=None, noisy=False, alpha=0.6,
                      points_alpha=0.3, labels=None, floor_plotter=None):

    if labels is None:
        if noisy:
            labels = pipe.data_provider.noisy_labels
        else:
            labels = pipe.data_provider.labels

    floor_plotter.draw_points(labels[:, 0],
                              labels[:, 1], color='grey', alpha=points_alpha)

    floor_plotter.draw_measurement_distribution(labels, alpha=alpha)

    if filename is not None:
        floor_plotter.filename = filename
        floor_plotter.save_plot()


def visualize_regression_accuracy(pipe: Pipeline, filename=None, floor_plotter=None, max_diff=None):
    if pipe.summary.y_pred[0].shape[1] != 2:
        log.error("Visualization only applicable for regression models")
        return

    diff = []
    plot_x = []
    plot_y = []

    for (y_true, y_pred) in zip(pipe.summary.y_true_labels, pipe.summary.y_pred):
        diff += np.linalg.norm(y_true - y_pred, axis=1).tolist()
        plot_x += y_pred[:, 0].tolist()
        plot_y += y_pred[:, 1].tolist()

    cm = plt.cm.get_cmap('RdYlBu_r')

    ax = floor_plotter.axis

    sc = ax.scatter(plot_x, plot_y, vmax=max_diff, c=diff, cmap=cm)

    divider = make_axes_locatable(ax)
    cax = divider.append_axes("right", size="5%", pad=0.05)

    plt.colorbar(sc, cax=cax)

    if filename is not None:
        floor_plotter.filename = filename
        floor_plotter.save_plot()

    plt.show()


def plot_data(pipe: Pipeline, filename=None, floor_plotter=None):
    """
    Plots the underlying data distribution of the il_pipeline on given
    floor plan
    """

    floor_plotter.draw_points(pipe.data_provider.labels[:, 0],
                              pipe.data_provider.labels[:, 1])

    if filename is not None:
        floor_plotter.filename = filename
        floor_plotter.save_plot()


if __name__ == "__main__":
    fp_img_file = resource_filename('data', 'lohan/CrowdsourcedDS1floor.png')


    file = "/home/laskama/PycharmProjects/loc_pipe_test/evaluation/test/output/kNN"
    pipe: Pipeline = Storable.load(file)

    fp = FloorplanPlot(
        floorplan_dimensions=(200, 80), grid_size=4,
        floorplan_bg_img=fp_img_file)

    visualize_regression_accuracy(pipe, floor_plotter=fp)

    p_file = resource_filename('il_pipeline', "data/test_pipeline")
    pipe: Pipeline = Storable.load(p_file)



    fp = FloorplanPlot(
        floorplan_dimensions=(200, 80), grid_size=4,
        floorplan_bg_img=fp_img_file)

    floorplan_plot(pipe, floor_plotter=fp, filename="../data/f1_score.pdf")

    fp = FloorplanPlot(
        floorplan_dimensions=(200, 80), grid_size=4,
        floorplan_bg_img=fp_img_file)

    plot_cluster_number(pipe, floor_plotter=fp)

    fp = FloorplanPlot(
        floorplan_dimensions=(200, 80), grid_size=4,
        floorplan_bg_img=fp_img_file)

    plot_data_heatmap(pipe, floor_plotter=fp, filename="../data/data_heatmap.pdf")

    fp = FloorplanPlot(
        floorplan_dimensions=(200, 80), grid_size=4,
        floorplan_bg_img=fp_img_file)

    plot_data(pipe, floor_plotter=fp)

    plt.show()