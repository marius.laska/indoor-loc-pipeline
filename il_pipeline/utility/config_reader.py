import os
import yaml
import copy

from il_pipeline.backend.backend_manager import BackendManager


class ConfigReader:

    def __init__(self, conf_file):
        self.conf_file = conf_file
        self.global_params = None
        self.connection_params = None
        self.rest_params = None
        self.dnn_params = None
        self.data_params = None
        self.main_params = None
        self.floor_plan_params = None
        self.output_params = None
        self.output_dir = None
        self.result_dir = None
        self.backend_manager: BackendManager = None
        self.cfg = None
        self.read_config_file()

    def get_params(self, key, local_params, global_params=None, merge_level=0):
        # either take global segmentation params or il_pipeline specific
        try:

            # if no global params supplied use the ones of class
            if global_params is None:
                global_params = self.global_params
                if global_params is None:
                    return local_params[key]

            # if a key is supplied as global parameters use this as subset
            if type(global_params) is str and self.global_params is not None:
                global_params = self.global_params[global_params]
            if global_params is None:
                return local_params[key]

            if key in local_params and local_params[key] is not None:
                params = local_params[key]

                while merge_level > 0:
                    if type(global_params[key]) is dict:
                        for g_k, g_v in global_params[key].items():
                            if type(local_params[key]) is dict and g_k not in local_params[key]:
                                local_params[key][g_k] = g_v
                        merge_level -= 1
            else:
                params = global_params[key]

        except KeyError:
            try:
                return local_params[key]
            except:
                return None

        return params

    def read_config_file(self):
        with open(self.conf_file, "r") as yml_file:
            cfg = yaml.load(yml_file)

            if 'global_params' in cfg:
                self.global_params = cfg['global_params']
            if 'dnn_params' in cfg:
                self.dnn_params = cfg['dnn_params']
            if 'data' in cfg:
                self.data_params = cfg['data']
                if 'REST_connection' in self.data_params:
                    self.rest_params = self.data_params['REST_connection']
            if 'main' in cfg:
                self.main_params = cfg['main']
            if 'floor_plan' in cfg:
                self.floor_plan_params = cfg['floor_plan']
            if 'output' in cfg:
                self.output_params = cfg['output']
            if self.output_params is not None and 'model_dir' in self.output_params:
                self.output_dir = self.output_params['model_dir']
            if self.output_params is not None and 'summary_dir' in self.output_params:
                self.result_dir = self.output_params['summary_dir']
            if 'connection' in cfg:
                self.connection_params = cfg['connection']
            if 'pipelines' in cfg:
                self.pipelines = cfg['pipelines']

            self.cfg = cfg

    def setup_directories(self):
        if self.output_dir is not None and not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        if self.result_dir is not None and not os.path.exists(self.result_dir):
            os.makedirs(self.result_dir)
        if self.output_params is not None and 'img_folder' in self.output_params and not os.path.exists(
                self.output_params['img_folder']):
            os.makedirs(self.output_params['img_folder'])

    def download_floor_plan(self):
        if self.backend_manager is None:
            if not self.create_backend_manager():
                return

        folder = self.output_params['img_folder']
        file = self.data_params['floor_plan_img']
        if not os.path.isfile(folder + file):
            self.backend_manager.download_floorplan()

    def create_backend_manager(self):
        if self.rest_params is None:
            return False

        self.backend_manager = BackendManager(
            self.rest_params['base_url'],
            self.rest_params['token'],
            map_id=self.data_params['map_id'],
            map_img_name=self.data_params['floor_plan_img'],
            map_folder=self.output_params['img_folder'])

        return True

    # Methods for generating flat pipeline configs from configs that contain
    # list parameters

    def get_expanded_dicts(self, p, p_orig, key=None):
        curr_dicts = p.copy()
        subsets = []
        keys = []

        for k, v in p_orig.items():

            if type(v) is dict:
                # nested
                subsets.append(k)

            if type(v) is list:

                n = len(v)
                # requires n copies of p_orig
                # p_cop = [p.copy() for _ in range(n)]
                l = []
                for c_d in curr_dicts:
                    for idx in range(n):
                        l.append(c_d.copy())

                keys.append(k)
                curr_dicts = l
                for c_idx, c_d in enumerate(curr_dicts):
                    c_d[k] = v[c_idx % n]
            else:
                for c_d in curr_dicts:
                    if key is None:
                        c_d[k] = v
                    else:
                        c_d[key][k] = v

        return curr_dicts, subsets, keys

    def expand_dict_with_sub_dicts(self, d_curr, d_sub, keys=[], dup_keys=[]):
        if len(d_sub) > 1:
            # copy d_curr len(d_2) times
            l = []

            for d_c in d_curr:
                for idx in range(len(d_sub)):
                    d = copy.deepcopy(d_c)
                    val = d_sub[idx].copy()

                    for d_idx, d_key in enumerate(dup_keys):
                        d["name"] = d["name"] + "_" + d_key + "_" + str(val[d_key])

                    if len(keys) == 1:
                        d[keys[0]] = val
                    if len(keys) == 2:
                        d[keys[0]][keys[1]] = val
                    if len(keys) == 3:
                        d[keys[0]][keys[1]][keys[2]] = val
                    else:
                        print("key length not supported, max length = 3")

                    l.append(d)

            d_curr = l.copy()

        return d_curr

    def get_pipeline_variation(self, pipelines):
        p_res = []

        for p in pipelines:
            d_1, keys, dup_keys = self.get_expanded_dicts([p], p)
            if len(d_1) > 1:
                print("test")
            d_curr = d_1
            for k_1 in keys:
                d_2, keys, dup_keys = self.get_expanded_dicts([p[k_1]], p[k_1])

                d_curr = self.expand_dict_with_sub_dicts(d_curr, d_2,
                                                         keys=[k_1], dup_keys=dup_keys)

                for k_2 in keys:
                    d_3, keys, dup_keys = self.get_expanded_dicts([p[k_1][k_2]],
                                                        p[k_1][k_2])

                    d_curr = self.expand_dict_with_sub_dicts(d_curr, d_3,
                                                             keys=[k_1, k_2], dup_keys=dup_keys)

                    for k_3 in keys:
                        d_4, keys, dup_keys = self.get_expanded_dicts([p[k_1][k_2][k_3]],
                                                            p[k_1][k_2][k_3])

                        d_curr = self.expand_dict_with_sub_dicts(d_curr, d_4,
                                                                 keys=[k_1, k_2,
                                                                       k_3], dup_keys=dup_keys)

            p_res += d_curr

        return p_res
