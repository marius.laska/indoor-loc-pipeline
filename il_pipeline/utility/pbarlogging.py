import tqdm
import time
import contextlib

from debug_tools import getLogger
log = getLogger()


class DummyTqdmFile(object):
    """ Dummy file-like that will write to tqdm
    https://github.com/tqdm/tqdm/issues/313
    """
    file = None

    def __init__(self, file):
        self.file = file

    def write(self, x):
        # Avoid print() second call (useless \n)
        if len(x.rstrip()) > 0:
            tqdm.tqdm.write(x, file=self.file, end='')

    def flush(self):
        return getattr(self.file, "flush", lambda: None)()

@contextlib.contextmanager
def std_out_err_redirect_tqdm(log):

    try:
        if log._file:
            original_file = log._file.stream
            log._file.stream = DummyTqdmFile( original_file )
            yield original_file

        elif log._stream:
            original_stream = log._stream.stream
            log._stream.stream = DummyTqdmFile( original_stream )
            yield original_stream

    except Exception as exc:
        raise exc

    finally:
        if log._file:
            log._file.stream = original_file
        elif log._stream:
            log._stream.stream = original_stream


if __name__ == "__main__":
    log.info("loop")

    outputstream = std_out_err_redirect_tqdm( log )

    pbar = tqdm.tqdm(total=20, file=outputstream, dynamic_ncols=True)

    for x in range(20):
        pbar.update()
        time.sleep(.1)

        if not x % 5:
            log.debug("in loop %d\nnew line" % x)

    log.info("done")