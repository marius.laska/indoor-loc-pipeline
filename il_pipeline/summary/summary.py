import numpy as np
from ldce.base import ClusterBase
from data.data_provider_base import DataProviderBase
from acs.metrics import class_f1


class KFoldClassSummary:
    """
    Stores data for k-fold evaluation of area classifier.
    Can be used to calculate ACS (for lazy parameterization)
    """

    def __init__(self, loc_scores=None, accuracies=None,
                 fmeasure_accuracies=None):
        self.loc_scores = loc_scores
        self.accuracies = accuracies
        self.fmeasure_accuracies = fmeasure_accuracies
        self.num_folds = 0
        self.fold_class_acc = []
        self.fold_class_fmeasure = []
        self.y_pred = []
        self.y_true = []
        self.y_true_labels = []


    def get_class_error_for_reg(self, data_provider: DataProviderBase, clusterer: ClusterBase, type="mean", model_type="class", dist_mode="center"):
        if model_type == "class":
            dists = self.get_misclass_reg_cdf(clusterer, dist_mode=dist_mode)
        if model_type == "reg":
            dists = self.get_reg_cdf(data_provider)
        if type == "mean":
            val = np.mean(dists)
        if type == "std":
            val = np.std(dists)
        if type == "max":
            val = np.max(dists)
        if type == "min":
            val = np.min(dists)

        return val

    def get_class_error(self, clusterer: ClusterBase, type="mean", dist_mode="center"):
        area_dist = clusterer.calc_area_center_dist(dist_mode=dist_mode)
        y_true_f = np.concatenate([y_true for y_true in self.y_true], axis=0)
        y_pred_f = np.concatenate([y_pred for y_pred in self.y_pred], axis=0)

        y_true_f = np.argmax(y_true_f, axis=1)
        y_pred_f = np.argmax(y_pred_f, axis=1)

        dists = area_dist[y_true_f, y_pred_f]

        if type == "mean":
            val = np.mean(dists)
        if type == "std":
            val = np.std(dists)
        if type == "max":
            val = np.max(dists)
        if type == "min":
            val = np.min(dists)

        return val

    def get_reg_cdf(self, data_provider: DataProviderBase):
        dists = []
        for f_idx, (y_pred, y_true_labels) in enumerate(zip(self.y_pred, self.y_true_labels)):
            # get y_true (x,y) labels

            non_nan_mask = np.where(~np.isnan(y_pred))[0]
            dist = np.linalg.norm(y_true_labels[non_nan_mask, :] - y_pred[non_nan_mask, :], axis=1)
            dists += dist.tolist()

        return dists

    def get_misclass_reg_cdf(self, clusterer: ClusterBase,
                             dist_mode="center",
                             add_penalty_for_missed_class=False):
        dist = []
        area_dist = clusterer.calc_area_center_dist(dist_mode=dist_mode)

        for f_idx, (y_true, y_pred) in enumerate(zip(self.y_true, self.y_pred)):
            pred_areas, _, coverage, mask = clusterer.get_cluster_labels_for_areas(
                y_pred, area_mode="convex_hull")
            no_match_mask = np.all(pred_areas == 0, axis=1)
            no_match_idx = np.where(no_match_mask)[0]
            match_idx = np.where(~no_match_mask)[0]

            # calc dist for predictions where any area is matched
            max_pred = np.argmax(pred_areas[match_idx], axis=1)
            max_true = np.argmax(y_true[match_idx], axis=1)

            diff = np.where(max_pred != max_true)[0]
            pred_diff = max_pred[diff]
            true_diff = max_true[diff]
            n_dist = area_dist[pred_diff, true_diff]
            dist += n_dist.tolist()

            num_true = len(max_true) - len(diff)
            # append zeros for correct areas
            dist += [0 for _ in range(num_true)]

            # calc dist for predictions where no area of segmentation matched
            closest_areas, distances = clusterer.get_closest_area_for_labels(y_pred[no_match_idx])
            max_true = np.argmax(y_true[no_match_idx], axis=1)
            n_dist = area_dist[closest_areas, max_true]
            if add_penalty_for_missed_class:
                n_dist += np.array(distances)

            dist += n_dist.tolist()

        return dist

    def get_misclass_cdf(self, clusterer: ClusterBase, dist_mode="center"):
        dist = []
        area_dist = clusterer.calc_area_center_dist(dist_mode=dist_mode)

        for f_idx, (y_true, y_pred) in enumerate(zip(self.y_true, self.y_pred)):
            max_pred = np.argmax(y_pred, axis=1)
            max_true = np.argmax(y_true, axis=1)

            diff = np.where(max_pred != max_true)[0]
            pred_diff = max_pred[diff]
            true_diff = max_true[diff]
            num_true = len(y_true) - len(diff)

            # append zeros for correct areas
            dist += [0 for _ in range(num_true)]

            n_dist = area_dist[pred_diff, true_diff]

            dist += n_dist.tolist()

        return dist

    def get_avg_loc_scores(self):
        test = np.array(self.loc_scores)
        return np.mean(test, axis=0)

    def get_avg_accuracy(self):
        return np.mean(np.array(self.accuracies))

    def get_avg_fmeasure(self, mask_unseen_classes=False, k_fold_avg="macro"):
        mask = [np.unique(np.argmax(y_true, axis=1)) for y_true in self.y_true]

        if k_fold_avg == "micro":
            y_pred = np.concatenate(self.y_pred, axis=0)
            y_true = np.concatenate(self.y_true, axis=0)

            f1_tensor = class_f1(y_true, y_pred)

            return f1_tensor

        if k_fold_avg == "macro":
            if self.fmeasure_accuracies is None or len(self.fmeasure_accuracies) == 0:
                f1_tensor = np.array(
                    [class_f1(y_true[:len(y_pred), :], y_pred) for (y_true, y_pred)
                     in zip(self.y_true, self.y_pred)])
                if mask_unseen_classes:
                    means = []
                    for r_idx, row in enumerate(f1_tensor):
                        means.append(np.mean(row[mask[r_idx]]))
                    return np.array(means)
                return np.mean(f1_tensor, axis=0)
        return np.mean(np.array(self.fmeasure_accuracies))


if __name__ == "__main__":
    pass
