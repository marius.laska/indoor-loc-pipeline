import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="indoor-loc-pipeline", # Replace with your own username
    version="0.0.1",
    author="Marius Laska",
    author_email="marius.laska@gia.rwth-aachen.de",
    description="Indoor localization pipeline...",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="git@git.rwth-aachen.de:marius.laska/indoor-loc-pipeline.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    include_package_data=True,
    python_requires='>=3.6',
    install_requires=['tensorflow==1.14.0', 'talos==0.4.8', 'psycopg2',
                      'debug_tools', 'gast==0.2.2', # important for tensorflow 1.14
                      'indoor-loc-data @ git+https://git.rwth-aachen.de/marius.laska/indoor-loc-data.git',
                      'indoor-loc-segmentation @ git+https://git.rwth-aachen.de/marius.laska/ldce-clustering.git',
                      'indoor-loc-acs @ git+https://git.rwth-aachen.de/marius.laska/area-classification-score.git']

)